import json
import unittest
import coverage

cov = coverage.coverage(branch=True)
cov.start()

from app import socketio
from app import app
from flask import Flask, request, json
from flask_socketio import SocketIO, send, emit, join_room, disconnect, leave_room

@socketio.on_error()
def error_handler(value):
    if isinstance(value, AssertionError):
        global error_testing
        error_testing = True
    else:
        raise value
    return value

@socketio.on('error_testing')
def raise_error(data):
    raise AssertionError()



class TestSocketIO(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        cov.stop()

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_connect(self):
        client = socketio.test_client(app)
        self.assertTrue(client.is_connected())

        client.disconnect()
        self.assertFalse(client.is_connected())

    def test_disconnect(self):
        disconnect = None
        client = socketio.test_client(app)
        client.disconnect()
        self.assertFalse(client.is_connected()) 

    def test_room(self):
        client = socketio.test_client(app)
        client.emit('join_room', {'room_id': 5, 'is_join': 0})
        received = client.get_received()
        self.assertEqual(received[0]['name'], 'greeting_join_room')
        self.assertEqual(received[0]['args'][0]['room_id'], 5)
        self.assertEqual(received[0]['args'][0]['is_join'], 1)
        self.assertEqual(received[0]['args'][0]['message'], 'Selamat datang di Chatbot ProsaAI')

    def test_send_chat(self):
        client = socketio.test_client(app)
        client.emit('join_room', {'room_id':5, 'is_join': 0})
        client.emit('send_chat', {'room_id': 5, 'chat': 'dimana'})
        received = client.get_received()
        self.assertEqual(received[1]['name'], 'receive_chat')
        self.assertEqual(received[1]['args'], ['disini'])

    def test_request_disconnect(self):
        client = socketio.test_client(app)
        client.emit('join_room', {'room_id': 5, 'is_join': 0})
        client.emit('request_disconnect', {'room_id': 5})
        self.assertFalse(client.is_connected()) 



if __name__ == '__main__':
    unittest.main()