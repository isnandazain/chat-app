from flask import Flask, render_template, redirect, url_for, request
from flask_socketio import SocketIO, join_room, close_room, disconnect

from flask_sqlalchemy import SQLAlchemy

from configuration import ProsaConfig
import os
import random
import Levenshtein

import base64
import hashlib
import hmac

# keperluan line bot
from linebot import LineBotApi
from linebot.models import TextSendMessage

app = Flask(__name__)
app.config.from_object(ProsaConfig)
db = SQLAlchemy(app)
socketio = SocketIO(app, ping_interval=1000)

# Deklarasi Tabel
class QAPairs(db.Model):
    __tablename__ = 'qa_pairs'

    id = db.Column(db.Integer, primary_key=True)

    question = db.Column(db.String(255), nullable=False)

    answer = db.Column(db.String(255), nullable=False)

    is_deleted = db.Column(db.Boolean, default=0)

    def __init__(self, question, answer):
        self.question = question
        self.answer = answer
        self.is_deleted = 0


@app.route("/callback", methods=["POST"])
def linebot_reply():
    app.logger.info("callback from line bot")

    signature = request.headers['X-Line-Signature']

    body = request.get_data(as_text=True)
    line_bot_api = LineBotApi(ProsaConfig.LINEBOT_CHANNEL_ACCESS_TOKEN)

    # signature validation
    hash_ = hmac.new(ProsaConfig.LINEBOT_CHANNEL_SECRET.encode('utf-8'),
                    body.encode('utf-8'), hashlib.sha256).digest()
    
    if not signature == base64.b64encode(hash_):
        os.abort(400)

    # choose answer
    setpairs = QAPairs.query.filter(
        QAPairs.question.like("%{}%".format(body.message.text))
    ).all()

    answer = ""
    best_distance = 9999
    if not setpairs:
        answer = "Jawaban tidak ditemukan, silahkan ajukan pertanyaan lainnya."

    for pair in setpairs:
        # calculate kemiripan
        distance = Levenshtein.distance(body.message.text, pair.question)
        if distance < best_distance:
            answer = pair.answer
            best_distance = distance

    # request reply message
    line_bot_api.reply_message(
        body.reply_token,
        TextSendMessage(text=answer)
    )

    return "Done"


@app.route("/", methods=["GET"])
def chat():
    return render_template("chat.html")


@socketio.on('join_room')
def join_room_event(data):
    app.logger.info("someone has joined the room")

    if data['is_join'] == 0:
        join_room(data['room_id'])

    # add data
    data['message'] = "Selamat datang di Chatbot ProsaAI"
    data['is_join'] = 1
    socketio.emit('greeting_join_room', data)


@socketio.on('request_disconnect')
def request_disconnect_event(data):
    app.logger.info("someone has disconnect request")

    # add data
    data['message'] = "Selamat Tinggal, Sampai Jumpa di Moment Selanjutnya"
    socketio.emit('goodbye_room', data)

    # disconnect
    close_room(data['room_id'])
    disconnect()

    
@socketio.on('send_chat')
def send_chat_event(data):
    app.logger.info("someone has send a chat")
    setpairs = QAPairs.query.filter(
        QAPairs.question.like("%{}%".format(data['chat']))
    ).all()

    answer = ""
    best_distance = 9999
    if not setpairs:
        answer = "Jawaban tidak ditemukan, silahkan ajukan pertanyaan lainnya."

    for pair in setpairs:
        # calculate kemiripan
        distance = Levenshtein.distance(data['chat'], pair.question)
        if distance < best_distance:
            answer = pair.answer
            best_distance = distance

    socketio.emit('receive_chat', answer, room=data['room_id'])


if __name__ == '__main__':
    socketio.run(app, debug=True)