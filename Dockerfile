FROM prairielearn/centos7-python

# directory for project
RUN mkdir /src
WORKDIR /src

# Copy requirements
ADD requirements.txt /src

# Install dependencies
RUN yum -y install \
        mariadb-devel \
        mariadb-libs \
        openjpeg-devel \	
        zlib \
        zlib-devel \
        pcre-devel \
    && pip3.6 install --no-cache-dir -r requirements.txt 
