# Chat-APP with Flask-SocketIO

# Python Package
pip install -r requirements.txt

# Run
python app.py

# Test
python test.py

# Mysql Config
DB_HOST=host<br/>
DB_USER=user<br/>
DB_PASS=pass<br/>
DB_NAME=db

# Penentuan Jawaban Dikirm
1. Ambil calon jawaban pada database menggunakan query LIKE berdasarkan pertanyaan yang diajukan
2. Hitung ratio kemiripan calon jawaban terhadap pertanyaan yang diajukan menggunakan fuzzywuzzy
3. Calon jawaban dengan kemiripan tertinggi yang dipilih


